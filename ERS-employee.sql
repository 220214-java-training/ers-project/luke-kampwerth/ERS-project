
create table employee(
		Employee_id serial primary key,
		user_name varchar(50) not null unique,
		email varchar(100) not null unique,
		pass_word varchar(50) not null,
		Manager boolean
);

create table reimbursement(
		Reimbursement_id serial primary key,
		amount_requested numeric(5,2),
		description varchar(500) not null,
		date_submitted date,
		status varchar(10)
);
select *
from employee; 

select *
from reimbursement; 
alter table reimbursement add column employee_id integer references employee(employee_id);
insert into employee(employee_id, user_name, email, pass_word, manager) values (1,'Frankie', 'frankie@org.com','frankie', true);
insert into employee(employee_id, user_name, email, pass_word, manager) values (2,'Frannie', 'frannie@org.com','frannie', false);
insert into employee(employee_id, user_name, email, pass_word, manager) values (3,'Dannie', 'dannie@org.com','dannie', false);
insert into employee(employee_id, user_name, email, pass_word, manager) values (4,'Dougie', 'dougie@org.com','dougie', false);
insert into employee(employee_id, user_name, email, pass_word, manager) values (5,'Sammy', 'sammy@org.com','sammy', false);
insert into employee(employee_id, user_name, email, pass_word, manager) values (6,'Donnie', 'donnie@org.com','donnie', false);
insert into employee(employee_id, user_name, email, pass_word, manager) values (7,'Johnnie', 'johnnie@org.com','johnnie', false);

