package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.daos.EmployeeDao;
import com.revature.daos.EmployeeDaoImpl;
import com.revature.models.Employee;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    private ObjectMapper om = new ObjectMapper();
    private EmployeeDao employeeDao = new EmployeeDaoImpl();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String usernameParam = req.getParameter("user_name");
        String passwordParam = req.getParameter("pass_word");


        if (usernameParam.isEmpty() || passwordParam.isEmpty()) {
            resp.sendError(500,"Either Username or Password is empty");
        } else
            System.out.println(usernameParam + passwordParam);
        List<Employee> employeeByUserAndPass = employeeDao.getEmployeeByUsernameAndPassword(usernameParam, passwordParam);
        System.out.println(employeeByUserAndPass);

        String employeeJSON = om.writeValueAsString(employeeByUserAndPass);

        if(employeeByUserAndPass.isEmpty()) {
            resp.sendError(500, "Invalid Username or Password");
        }else
            if(employeeJSON.contains("true")) {
        resp.sendRedirect("http://localhost:8080/ExpenseReimbursementSystem/views/manager/all-reimbursements.html");
    }
    else {
        resp.sendRedirect("http://localhost:8080/ExpenseReimbursementSystem/views/employee/new-reimbursements.html");
    }
}





    }

