package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.daos.ReimbursementDao;
import com.revature.daos.ReimbursementDaoImpl;
import com.revature.models.Reimbursement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
@WebServlet(urlPatterns = "/reimbursements")
public class ReimbursementServlet extends HttpServlet {
    private ObjectMapper om = new ObjectMapper();

    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reimbursementIdParam = req.getParameter("reimbursement_id");
        String statusParam = req.getParameter("status");

        if(reimbursementIdParam != null && statusParam != null){
            List<Reimbursement> updateByReimbursementByStatusAndId = reimbursementDao.updateByReimbursementByStatusAndId(Integer.parseInt(reimbursementIdParam), statusParam);
            String json = om.writeValueAsString(updateByReimbursementByStatusAndId);
            resp.setHeader("Content-type","application/json");
            try (PrintWriter pw = resp.getWriter()){
                pw.write(json);

            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


           String statusParam = req.getParameter("status");
           System.out.println(statusParam);

           String paramJSON = om.writeValueAsString(statusParam);



           if(paramJSON.contains("PENDING")){
               List<Reimbursement> getReimbursementsByStatus = reimbursementDao.getReimbursementByStatus(statusParam);
               String pendingJSON = om.writeValueAsString(getReimbursementsByStatus);
               resp.setHeader("Content-type","application/json");
               try (PrintWriter pw = resp.getWriter()){
                   pw.write(pendingJSON);

               }
           } else if(paramJSON.contains("APPROVED")){
               List<Reimbursement> getReimbursementsByStatus = reimbursementDao.getReimbursementByStatus(statusParam);
               String approvedJSON = om.writeValueAsString(getReimbursementsByStatus);
               resp.setHeader("Content-type","application/json");
               try (PrintWriter pw = resp.getWriter()){
                   pw.write(approvedJSON);

               }

           } else if(paramJSON.contains("DENIED")) {
               List<Reimbursement> getReimbursementsByStatus = reimbursementDao.getReimbursementByStatus(statusParam);
               String deniedJSON = om.writeValueAsString(getReimbursementsByStatus);
               resp.setHeader("Content-type","application/json");
               try (PrintWriter pw = resp.getWriter()){
                   pw.write(deniedJSON);

               }
           } else {


               List<Reimbursement> getAllReimbursements = reimbursementDao.getAllReimbursements();
               String reimbursementJSON = om.writeValueAsString(getAllReimbursements);
               resp.setHeader("Content-type", "application/json");
               try (PrintWriter pw = resp.getWriter()) {
                   pw.write(reimbursementJSON);
                   System.out.println(reimbursementJSON);

               }


           }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {



        String amountString = req.getParameter("amount_requested");
        Integer amount = Integer.parseInt(amountString);

        String description = req.getParameter("description");
        String dateString = req.getParameter("date_submitted");
        LocalDate date = LocalDate.parse(dateString);
        String status  = req.getParameter("status");
        String employeeIdString = req.getParameter("employee_id");
        Integer employeeId = Integer.parseInt(employeeIdString);




        Reimbursement newReimbursement = new Reimbursement();
        newReimbursement.setAmount(amount);
        newReimbursement.setDescription(description);
        newReimbursement.setDate(date);
        newReimbursement.setStatus("PENDING");
        newReimbursement.setEmployeeId(employeeId);









            boolean successfulSubmission = reimbursementDao.createReimbursement(newReimbursement);
            if(successfulSubmission){
                resp.setStatus(201, "Submission successful");
                resp.sendRedirect("http://localhost:8080/ExpenseReimbursementSystem/views/employee/Successful-submission.html");
            } else{
                resp.sendError(500);
            }
        }
    }
