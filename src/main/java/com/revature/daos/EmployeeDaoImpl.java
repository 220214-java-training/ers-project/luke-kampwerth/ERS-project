package com.revature.daos;

import com.revature.models.Employee;
import com.revature.connection.ConnectionUtil;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class EmployeeDaoImpl implements EmployeeDao {
    @Override
    public List<Employee> getEmployeeByUsernameAndPassword(String username, String password) {
        String ps = "select * from employee where user_name = ? and pass_word = ? ";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ps)) {
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Employee> employeeList = new ArrayList<>();
            List<Employee> managerList = new ArrayList<>();


            while (resultSet.next()) {
                int employeeId = resultSet.getInt("employee_id");
                username = resultSet.getString("user_name");
                String email = resultSet.getString("email");
                password = resultSet.getString("pass_word");
                boolean manager = resultSet.getBoolean("manager");


                    Employee b = new Employee(employeeId, username, manager, password, email);
                    employeeList.add(b);
                    return employeeList;



            }

        }catch(SQLException e){
                e.printStackTrace();
        }
        return null;
    }



}