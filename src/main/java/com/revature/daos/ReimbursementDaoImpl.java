package com.revature.daos;

import com.revature.connection.ConnectionUtil;
import com.revature.models.Reimbursement;

import javax.swing.plaf.nimbus.State;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao {
    @Override


    public List<Reimbursement> getAllReimbursements(){
        String ps = "select * from reimbursement";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement =  connection.prepareStatement(ps)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            List<Reimbursement> reimbursements = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt("reimbursement_id");
                String description = resultSet.getString("description");
                int amount = resultSet.getInt("amount_requested");
                LocalDate date = resultSet.getObject("date_submitted", LocalDate.class);
                String status = resultSet.getString("status");
                int employeeId = resultSet.getInt("employee_id");
                Reimbursement b = new Reimbursement(id, description, amount, date, status, employeeId);
                reimbursements.add(b);

            }
            return reimbursements;
        } catch (SQLException e ){
            e.printStackTrace();
        }
        return null;

    }

    @Override
    public boolean createReimbursement(Reimbursement reimbursement) {
   //     if (reimbursement.getStatus() == null || reimbursement.getStatus().isEmpty()) {
     //       return false;
       // }
        try(Connection connection = ConnectionUtil.getConnection();){
            PreparedStatement ps = connection.prepareStatement("insert into reimbursement values (default, ?, ?, ?, ?, ?)");

                ps.setInt(1, reimbursement.getAmount());
                ps.setString(2, reimbursement.getDescription());
                ps.setObject(3, reimbursement.getDate());
                ps.setString(4, reimbursement.getStatus());
                ps.setInt(5, reimbursement.getEmployeeId());
                int rowsAffected = ps.executeUpdate();
                if (rowsAffected == 1) {
                    return true;
                }
            }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public List<Reimbursement> getReimbursementByStatus(String status) {
        String ps = "select * from reimbursement where status = ?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement statement = connection.prepareStatement(ps)) {

                 statement.setString(1,status);
                ResultSet resultSet = statement.executeQuery();
                List<Reimbursement> reimbursements = new ArrayList<>();
                while (resultSet.next()) {
                    int id = resultSet.getInt("reimbursement_id");
                    String description = resultSet.getString("description");
                    int amount = resultSet.getInt("amount_requested");
                    LocalDate date = resultSet.getObject("date_submitted", LocalDate.class);
                    status = resultSet.getString("status");
                    int employeeId = resultSet.getInt("employee_id");
                    Reimbursement b = new Reimbursement(id, description, amount, date, status, employeeId);
                    reimbursements.add(b);

                }
                return reimbursements;






        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Reimbursement> updateByReimbursementByStatusAndId(int id, String status) {
        String ps = "update reimbursement set status = ? where reimbursement_id = ?";
        try (Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ps)){
           System.out.println(preparedStatement);

            preparedStatement.setString(1, status);
            preparedStatement.setInt(2,id);

            preparedStatement.executeUpdate();




        } catch (SQLException e) {
            e.printStackTrace();
        }

    return null;}


}
