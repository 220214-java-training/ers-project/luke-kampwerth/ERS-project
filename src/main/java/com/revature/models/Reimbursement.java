package com.revature.models;
import java.time.LocalDate;
import java.util.Objects;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;


public class Reimbursement {
    private int id;
    private int amount;
    private String description;
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate date;
    private String status;
    private int employeeId;

    public Reimbursement(){
        super();
    }


    public Reimbursement(int id, String description, int amount, LocalDate date, String status, int employeeId) {
        super();
        this.id = id;
        this.amount = amount;
        this.description = description;
        this.date = date;
        this.status = status;
        this.employeeId = employeeId;


        // TODO: Define Reimbursement Model
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public boolean equals(Object o) {
       if ( this == o) return true;
       if (o == null || getClass() != o.getClass()) return false;
       Reimbursement reimbursement = (Reimbursement) o;
       return id == reimbursement.id && Objects.equals(amount, reimbursement.amount) && Objects.equals(description, reimbursement.description) && Objects.equals(date,reimbursement.date) && Objects.equals(status, reimbursement.status) && Objects.equals(employeeId, reimbursement.employeeId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, amount, description, date, status, employeeId);

    }

    @Override
    public String toString() {
        return "Reimbursement {" + " Reimbursement Id= " + id + " Amount Requested= " + amount + " Description="+description+ " Date Submitted= " + date + " Status= " + status + " Employee Id= " + employeeId + '}';
    }



}
