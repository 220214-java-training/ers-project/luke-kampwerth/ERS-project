package com.revature.models;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionTest {
    public static void main(String args[]) {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://ers.cnzfhxjaudhj.us-east-1.rds.amazonaws.com:5432/ERS",
                            "Kampy", "Knights65");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
}
