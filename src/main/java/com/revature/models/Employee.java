package com.revature.models;
import java.util.Objects;

public class Employee {
    private int employeeId;
    private String username;
    private String password;
    private String email;
    private boolean manager;

    public Employee (){
        super();
    }


    public Employee(int employeeId, String username, boolean manager, String password, String email){
        super();
        this.employeeId = employeeId;
        this.username = username;
        this.password = password;
        this.email = email;
        this.manager = manager;

    }



    public int getEmployeeId() {
        return employeeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }




















    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeId == employee.employeeId && Objects.equals(username, employee.username) && Objects.equals(password, employee.password) && Objects.equals(email, employee.email) && Objects.equals(manager, employee.manager);
    }
    @Override
    public int hashCode(){
        return Objects.hash(employeeId, username,password, email, manager);
    }
    @Override
        public String toString() {
        return "Username = "+username+" Password = "+ password+ " EmployeeId = "+employeeId+" Email = "+email+ " Manager = "+manager;
    }

    }

